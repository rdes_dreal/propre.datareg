#' datareg : recuperer les donnees de reference d'une region
#'
#' @param code_reg character, le code insee de la region desiree
#'
#' @return un dataframe d'une ligne portant sur la region desiree
#' @importFrom attempt stop_if
#' @importFrom dplyr filter
#' @importFrom glue glue
#' @importFrom rlang .data
#' @export
#'
#' @examples
#' datareg("52")

datareg <- function(code_reg = "52") {
  attempt::stop_if(!(code_reg %in% propre.datareg::banque_regionale$code_region),
                   msg = glue::glue("le code region {code_reg} est absent de la banque de donnees de reference. "))
  dplyr::filter(propre.datareg::banque_regionale, .data$code_region == code_reg)
}
