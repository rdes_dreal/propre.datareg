## code to prepare `logo` dataset goes here
library(hexSticker)
library(COGiter)
library(ggplot2)
library(sf)
library(dplyr)
p <- regions_geo %>%
  filter(as.numeric(as.character(REG)) > 10) %>%
  ggplot() +
  geom_sf(color = "cadetblue4", size = 0.5, fill = "azure") +
  theme_void() + theme_transparent()
p

sticker(p, package="propre.datareg",
        s_width = 0.95,
        s_height  = 0.95,
        p_size=20, s_x=1, s_y=.75,
        filename="inst/logo.png",
        p_color	="cadetblue4",
        dpi = 400,
        h_fill = "cornsilk",
        h_color = "cadetblue4",
        white_around_sticker = TRUE,
        spotlight = FALSE)
