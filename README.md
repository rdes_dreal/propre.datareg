Projet migré vers l'instance : https://gitlab-forge.din.developpement-durable.gouv.fr/dreal-pdl/csd/
<!-- README.md is generated from README.Rmd. Please edit that file -->

# propre.datareg

<!-- badges: start -->

<!-- badges: end -->

Le but de `{propre.datareg}` est de rendre disponible la banque de
formulations régionales et de coordonnées des DREALs via un package R.
Cette banque est issue d’un tableur collaboratif gooogle sheet. Elle
sert à désigner les régions dans les fonctions de verbatim des
publications statistiques reproductibles des DREAL (publications méthode
Propre) et à rédiger les mentions légales des publications.

## Installation

Vous pouvez installer {propre.datareg} depuis Gitlab avec :

``` r
remotes::install_gitlab("rdes_dreal/propre.datareg")
```

## Exemple

Les données sont contenue dans le dataset `banque_regionale` :

``` r
library(propre.datareg)
banque_regionale
#> # A tibble: 18 x 18
#>    code_region nom_region   en_de_nom_region   en_nom_region  dans_la_region_no~
#>    <chr>       <chr>        <chr>              <chr>          <chr>             
#>  1 01          Guadeloupe   en Guadeloupe      en Guadeloupe  en Guadeloupe     
#>  2 02          Martinique   en Martinique      en Martinique  en Martinique     
#>  3 03          Guyane       en Guyane          en Guyane      en Guyane         
#>  4 04          La Réunion   de La Réunion      à La Réunion   à La Réunion      
#>  5 06          Mayotte      de Mayotte         à Mayotte      à Mayotte         
#>  6 11          Île-de-Fran~ en Île-de-France   en Île-de-Fra~ dans la région Îl~
#>  7 24          Centre-Val ~ en Centre-Val de ~ en Centre-Val~ dans la région Ce~
#>  8 27          Bourgogne-F~ en Bourgogne-Fran~ en Bourgogne-~ dans la région Bo~
#>  9 28          Normandie    en Normandie       en Normandie   dans la région No~
#> 10 32          Hauts-de-Fr~ en Hauts-de-France dans les Haut~ dans la région Ha~
#> 11 44          Grand Est    en Grand Est       en région Gra~ dans la région Gr~
#> 12 52          Pays de la ~ en Pays de la Loi~ en Pays de la~ dans la région Pa~
#> 13 53          Bretagne     en Bretagne        en Bretagne    dans la région Br~
#> 14 75          Nouvelle-Aq~ en Nouvelle-Aquit~ en Nouvelle-A~ dans la région No~
#> 15 76          Occitanie    en Occitanie       en Occitanie   dans la région Oc~
#> 16 84          Auvergne-Rh~ en Auvergne-Rhône~ en Auvergne-R~ dans la région Au~
#> 17 93          Provence-Al~ en Provence-Alpes~ en Provence-A~ dans la région Pr~
#> 18 94          Corse        en Corse           en Corse       dans la région Co~
#> # ... with 13 more variables: de_la_region_nom_region <chr>, regional_e <chr>,
#> #   de_la_region <chr>, dans_la_region <chr>, la_region <chr>, nom_dreal <chr>,
#> #   adresse <chr>, telephone <chr>, courriel_contact <chr>,
#> #   prenom_directeur_directrice <chr>, nom_directeur_directrice <chr>,
#> #   directeur_regional_directrice_regionale <chr>, titre <chr>
```

Le package ne contient pour l’instant que deux fonctions :

  - `datareg()` qui filtre le dataset sur une région à partir de son
    code INSEE,  
  - `maj1let()` qui met la 1ère lettre du premier mot d’une chaîne de
    texte en majuscule. .

<!-- end list -->

``` r
library(propre.datareg)
data_bretagne <- datareg(code_reg = "53")
maj1let(data_bretagne$en_de_nom_region)
#> [1] "En Bretagne"
```

A terme, le package devrait contenir une autre fonction qui permet de
rapatrier les données à jour. Aujourd’hui, la mise à jour des données se
fait en mettant à jour le package à partir en refaisant tourner le
script `banque_regionale.R` présent dans data-raw.
